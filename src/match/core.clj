(ns match.core
  (:gen-class))

(require '[clojure.string :as str])
(require '[clojure.java.io :as io])
(require '[clj-time.coerce :as c])
(require '[clj-time.core :as t])

(defn- list-files
  [subdir]
  ; (rest)ing to drop first file as it is a folder itself
  ; TODO: filter isDir or smth
  (->> (str "labelled/" subdir) (io/resource) (io/file) (file-seq) (rest)))

(def nurse-and-date-length (count "N013_20140217"))

(def labels-files (list-files "labels"))

(def sensors-files (list-files "sensors"))

(def mslurp (memoize slurp))

(defn process-labels-file
  "returns structured data for which any activity id exists within the file"
  [file]
  (->> file
       (slurp)
       (#(str/split % #"\r\n"))
       (rest) ; removing table header that contains column description
       (map #(str/split % #","))
       (filter #(not= (first %) ""))))

(defn process-labels-files
  "[file-name [row]]"
  [files]
  (map (juxt #(.getName %) process-labels-file) files))

(defn dumb-to-date-time
  [dumb-date dumb-time]
  (let [date (str
              (subs dumb-date 0 4) "-"
              (subs dumb-date 4 6) "-"
              (subs dumb-date 6 8))
        time (->> dumb-time
                  (partition 2)
                  (map #(apply str %))
                  (interpose ":")
                  (apply str))]
    (c/to-date-time (str date " " time))))

(defn- -times-from-file
  [file]
  (let [file-name (.getName file)
        file-date (subs file-name 5 nurse-and-date-length)
        start-time (subs file-name (inc nurse-and-date-length) 20)
        end-time (subs file-name 21 27)
        file-start (dumb-to-date-time file-date start-time)
        file-end (dumb-to-date-time file-date end-time)]
    [file-start file-end]))

(def times-from-file (memoize -times-from-file))

(defn- -in-time-range
  "true if times represented by row and sensor-file intersect"
  [row sensor-file]
  (let [row-start (c/to-date-time (nth row 2))
        row-end (c/to-date-time (nth row 3))
        times (times-from-file sensor-file)
        file-start (first times)
        file-end (second times)]
    (some? (t/overlap
            (t/interval row-start row-end)
            (t/interval file-start file-end)))))

(def in-time-range (memoize -in-time-range))

(defn- within
  "point + offset is withing [start;end]"
  [from to time millis-wrapper]
  (let [millis (->> millis-wrapper (first) (read-string) (* 1000))]
    (t/within?
     (t/interval from to)
     (t/plus time (t/millis millis)))))

(defn process-sensor-file
  [row file]
  (let [activity-id (first row)
        activity-name (second row)
        ; TODO: copypasta
        row-start (c/to-date-time (nth row 2))
        row-end (c/to-date-time (nth row 3))
        times (times-from-file file)
        file-start (first times)
        file-end (second times)]
    (->> file
         (slurp)
         (#(str/split % #"\r\n"))
         (rest) ; table header comes first
         (map #(str/split % #","))
         (filter (fn [line] (within row-start row-end file-start line)))
         (map rest)
         (map #(concat [activity-id activity-name] %)))))

(defn match-for-row
  [row matching-files]
  (->> matching-files
       (filter #(in-time-range row %))
       (map #(process-sensor-file row %)))
  ; for every matching file include a line within time
)

(defn match-for-file
  [[name rows]]
  (let [matching-files (filter
                        #(=
                          (subs (.getName %) 0 nurse-and-date-length)
                          (subs name 0 nurse-and-date-length))
                        sensors-files)]
    (->> rows
         (map #(match-for-row % matching-files)))))

(defn match-with-sensors
  [processed-labels]
  (->> processed-labels
       (map match-for-file)
       (filter #(pos? (count %)))
       (flatten)))

(defn -main
  [& args]
  (->> labels-files
       (process-labels-files)
       (match-with-sensors)
       (partition 11)
       ;(map #(into [(first %)] (rest (rest %))))
       (group-by first)
       (vals)
       (map (take 100))
       (apply concat)
       (map #(str/join "," %))
       ;(take 500)
       (map println)
       (doall)))
